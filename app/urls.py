from django.urls import path

from .views import app_detail, AppView2



urlpatterns = [
    path('app/', app_detail),
    path('app1/', AppView2.as_view()),

]
