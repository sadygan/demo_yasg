from django.shortcuts import render
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import api_view, list_route, detail_route
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework import viewsets, generics
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from app.models import App


class ASerrializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=False, allow_blank=True, max_length=100)
    linenos = serializers.BooleanField(required=False)


# class AppView3(generics.ListAPIViewset):
#     @swagger_auto_schema(responses={200: 'asd'})
#     def get(self, request, format=None):
#         pass


class AppSerializer(serializers.ModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = App
        fields = '__all__'


test_param = openapi.Parameter('test',
                               openapi.IN_PATH,
                               description="test manual param",
                               type=openapi.TYPE_ARRAY,
                               items=openapi.Items(type='array', format='number',
                                                   enum=[1,2,3]
                                                   )
                               )
user_response = openapi.Response('11111response description', AppSerializer)


def _404():
    return 'not found'
RESP_TEXT = {
    '404': '111Not found'
}
RESP_MES = (
    ('404', 'eeeerrrr')
)


def partial_update(self, request, *args, **kwargs):
   """partial_update method docstring"""

@swagger_auto_schema(method='get',
                     manual_parameters=[test_param],
                     tags=['testtag',]
                     )
@swagger_auto_schema(methods=['put', 'post'], request_body=AppSerializer)
@api_view(['GET', 'PUT', 'POST'])
def app_detail(self, request):
    pass

class AppView(viewsets.ModelViewSet):
    serializer_class = AppSerializer
    @swagger_auto_schema(operation_description='GET /articles/today/')
    @list_route(methods=['get'])
    def today(self, request):
        pass

    @swagger_auto_schema(method='get', operation_description="GET /articles/{id}/image/")
    @swagger_auto_schema(method='post', operation_description="POST /articles/{id}/image/")
    @detail_route(methods=['get', 'post'], parser_classes=(MultiPartParser,))
    def image(self, request, id=None):
        pass

    @swagger_auto_schema(operation_description="PUT /articles/{id}/")
    def update(self, request, *args, **kwargs):
        pass

    @swagger_auto_schema(operation_description="PATCH /articles/{id}/")
    def partial_update(self, request, *args, **kwargs):
        pass


class AppView2(APIView):
    @swagger_auto_schema(responses={200: AppSerializer(many=True)})
    def get(self, request, format=None):
        pass


